﻿using System;

namespace Ponger
{
    class Program
    {
        static void Main(string[] args)
        {
            Ponger ponger = new Ponger();
            ponger.StartListening();

            Console.ReadKey(true);
        }
    }
}
