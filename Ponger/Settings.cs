namespace Ponger
{
    public static class Settings
    {
        public const string LISTEN_QUEUE_NAME = "pong_queue";
        public const string LISTEN_QUEUE_KEY = "pong";
        public const string SENDING_QUEUE_KEY = "ping";
    }
}