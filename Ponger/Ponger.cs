using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper;
using static Ponger.Settings;

namespace Ponger
{
    public sealed class Ponger
    {
        private readonly Cui _cui;
        private readonly IRabbitWrapper _rabbitWrapper;
        public Ponger()
        {
            _cui = new Cui();
            _rabbitWrapper = new RabbitWrapper(LISTEN_QUEUE_NAME, LISTEN_QUEUE_KEY, SENDING_QUEUE_KEY);
        }
        public void StartListening()
        {
            _rabbitWrapper.ListenQueue();

            _cui.ShowStartingDialog();
            _cui.ShowEndingCaseNotification();
        }
    }
}