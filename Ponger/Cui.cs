using System;
using static Ponger.Settings;

namespace Ponger
{
    public sealed class Cui
    {
        public void ShowStartingDialog()
        {
            Console.WriteLine(
                "====================PONGER====================\n" +
                $"\tListening at the queue with name {LISTEN_QUEUE_NAME} has been started" 
            );
        }

        public void ShowEndingCaseNotification()
        {
            Console.WriteLine("Press any key to close the program");
        }
    }
}