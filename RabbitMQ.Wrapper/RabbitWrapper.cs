﻿using System;
using System.Threading;
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Interfaces;
using static RabbitMQ.Wrapper.Settings;

namespace RabbitMQ.Wrapper
{
    public class RabbitWrapper : IRabbitWrapper
    {
        private readonly string _listenQueueName;

        private readonly string _listenQueueKey;
        private readonly string _sendingQueueKey;

        private IConnection _listenConnection;
        private IModel _listenChannel;
        private EventingBasicConsumer _consumer;

        public RabbitWrapper(string listenQueueName, string listenQueueKey, string sendingQueueKey)
        {
            _listenQueueName = listenQueueName;

            _listenQueueKey = listenQueueKey;
            _sendingQueueKey = sendingQueueKey;
        }

        public void ListenQueue()
        {
            var factory = new ConnectionFactory()
            {
                Uri = new Uri(RABBITMQ_URI)
            };

            _listenConnection = factory.CreateConnection();
            _listenChannel = _listenConnection.CreateModel();
            
            _listenChannel.ExchangeDeclare(EXCHANGE_NAME, ExchangeType.Direct);

            _listenChannel.QueueDeclare(
                queue: _listenQueueName,
                durable: true,
                exclusive: false,
                autoDelete: false
            );
            _listenChannel.QueueBind(_listenQueueName, EXCHANGE_NAME, _listenQueueKey);

            _consumer = new EventingBasicConsumer(_listenChannel);
            _consumer.Received += MessageReceived;

            _listenChannel.BasicConsume(
                queue: _listenQueueName,
                autoAck: false,
                consumer: _consumer
            );
        }

        private void MessageReceived(object sender, BasicDeliverEventArgs args)
        {
            var body = args.Body;
            var message = Encoding.UTF8.GetString(body.ToArray());

            Console.WriteLine($"A message`s been received at {DateTime.Now.ToString()}: {message}");
            _listenChannel.BasicAck(args.DeliveryTag, false);

            Thread.Sleep(DELAY_INTERVAL);
            SendMessageToQueue(message);
        }

        public void SendMessageToQueue(string message)
        {
            var factory = new ConnectionFactory()
            {
                Uri = new Uri(RABBITMQ_URI)
            };

            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare(EXCHANGE_NAME, ExchangeType.Direct);

                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(
                    exchange: EXCHANGE_NAME,
                    routingKey: _sendingQueueKey,
                    basicProperties: null,
                    body: body
                );

                channel.Close();
                connection.Close();
            }
        }

        public void Dispose()
        {
            _listenChannel.Close();
            _listenConnection.Close();

            _listenChannel.Dispose();
            _listenConnection.Dispose();
        }
    }
}
