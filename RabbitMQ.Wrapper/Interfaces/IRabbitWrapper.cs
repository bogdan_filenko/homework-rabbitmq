using System;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IRabbitWrapper : IDisposable
    {
        void ListenQueue();
        void SendMessageToQueue(string message);
    }
}