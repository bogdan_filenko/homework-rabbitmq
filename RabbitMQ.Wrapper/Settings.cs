namespace RabbitMQ.Wrapper
{
    public static class Settings
    {
        public const int DELAY_INTERVAL = 2500;
        public const string EXCHANGE_NAME = "PingPongExchange";
        public const string RABBITMQ_URI = "amqp://guest:guest@localhost:5672";
    }
}