using System;

namespace Pinger
{
    public sealed class Cui
    {
        public void ShowStartingDialog()
        {
            Console.WriteLine(
                "====================PINGER====================\n" +
                "\tHello! Please, type your message to broadcast it"
            );
        }

        public void ShowEndingCaseNotification()
        {
            Console.WriteLine("Press any key to close the program");
        }

        public void ShowMessage(string message)
        {
            Console.WriteLine(message);
        }
    }
}