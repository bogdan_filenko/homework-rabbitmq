﻿using System;

namespace Pinger
{
    class Program
    {
        static void Main()
        {
            Pinger pinger = new Pinger();
            
            pinger.GetUserMessage();
            pinger.StartListening();
            
            Console.ReadKey(true);
        }
    }
}
