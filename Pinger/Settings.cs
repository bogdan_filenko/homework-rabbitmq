namespace Pinger
{
    public static class Settings
    {
        public const string LISTEN_QUEUE_NAME = "ping_queue";
        public const string LISTEN_QUEUE_KEY = "ping";
        public const string SENDING_QUEUE_KEY = "pong";
    }
}