using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper;
using System;
using static Pinger.Settings;

namespace Pinger
{
    public sealed class Pinger
    {
        private readonly Cui _cui;
        private readonly IRabbitWrapper _rabbitWrapper;
        public Pinger()
        {
            _cui = new Cui();
            _rabbitWrapper = new RabbitWrapper(LISTEN_QUEUE_NAME, LISTEN_QUEUE_KEY, SENDING_QUEUE_KEY);
        }
        public void GetUserMessage()
        {
            _cui.ShowStartingDialog();

            string message = Console.ReadLine();
            _rabbitWrapper.SendMessageToQueue(message);

            _cui.ShowEndingCaseNotification();
        }
        public void StartListening()
        {
            _rabbitWrapper.ListenQueue();
            _cui.ShowMessage($"Listening at the queue with name {LISTEN_QUEUE_NAME} has been started");
        }
    }
}